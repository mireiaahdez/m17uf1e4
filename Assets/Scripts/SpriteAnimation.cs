﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{

    public Transform playerTransform;
    public DataPlayer dataPlayer;

    public Sprite[] Sprites = new Sprite[3];


    public bool isMoving;
    public float distanceX;
    public float distanceY;

    public SpriteRenderer spriteRenderer;

    int spriteIndex = 0;

    private bool direction = true;


    // Start is called before the first frame update
    void Start()
    {

        dataPlayer = gameObject.GetComponent<DataPlayer>();


        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = Sprites[0];

        Application.targetFrameRate = 12;

        distanceX = 0;
        distanceY = 0;


        direction = true;


        if (dataPlayer.Weight <= 15f)
        {
            dataPlayer.Speed = 0.9f;
        }
        else if (dataPlayer.Weight <= 30f)
        {
            dataPlayer.Speed = 0.3f;
        }
        else if (dataPlayer.Weight <= 45f)
        {
            dataPlayer.Speed = 0.1f;
        }
        else
        {
            dataPlayer.Speed = 0.01f;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (direction)
        {
            if (isMoving == true)
            {

                spriteRenderer.sprite = Sprites[spriteIndex];

                if (spriteIndex == 2)
                {
                    spriteIndex = 1;
                }
                else
                {
                    spriteIndex++;
                }

                if (distanceX >= dataPlayer.distance)
                {
                    spriteRenderer.flipX = true;
                    spriteRenderer.sprite = Sprites[0];
                    direction = false;

                    distanceX = 0;
                }
                else
                {
                    transform.position = new Vector3(transform.position.x - dataPlayer.Speed, transform.position.y);
                    distanceX += dataPlayer.Speed;
                }
            }
            else
            {
                Debug.Log(spriteRenderer.sprite);
                spriteRenderer.sprite = Sprites[0];
            }
        }
        else
        {
            if (isMoving == true)
            {
                spriteRenderer.sprite = Sprites[spriteIndex];

                if (spriteIndex == 2)
                {
                    spriteIndex = 1;
                }
                else
                {
                    spriteIndex++;
                }

                if (distanceY >= dataPlayer.distance)
                {
                    spriteRenderer.flipX = false;
                    spriteRenderer.sprite = Sprites[0];
                    direction = true;

                    distanceY = 0;
                }
                else
                {
                    transform.position = new Vector3(transform.position.x + dataPlayer.Speed, transform.position.y);
                    distanceY += dataPlayer.Speed;
                }
            }
            else
            {
                spriteRenderer.sprite = Sprites[0];
            }
        }
    }


    

    
}
