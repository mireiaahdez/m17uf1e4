﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerType
{
    Sorcerer,
    Knight,
    Assesin,
    Bard,
    Barbarian
}
public class DataPlayer : MonoBehaviour
{

    [SerializeField]
    private string Name;
    public string Surname;
    public double Height;
    public float Weight;
    public float Speed = 10f;
    public float WalkDistance;
    public int lifes;
    public float distance;




    private bool isMoving;
    private Vector3 movingLeft;
    private Vector3 movingRight;


    public PlayerType Kind;

    public Sprite[] Sprites = new Sprite[3];


    // Start is called before the first frame update
    void Start()
    {

        Debug.Log($"Hola {name}!");

        GameObject suelo = GameObject.CreatePrimitive(PrimitiveType.Plane);
        suelo.transform.position = new Vector3(0.0f, -1.0f, 0.0f);

        movingLeft = new Vector3(-WalkDistance, transform.position.y, 0.0f);
        movingRight = new Vector3(WalkDistance, transform.position.y, 0.0f);

        isMoving = true;


    }

    // Update is called once per frame
    void Update()
    {

       if (isMoving == true)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            transform.Translate(Vector3.left * Time.deltaTime * Speed / (Weight * 10));
            if(transform.position.x <= movingLeft.x)
            {
                isMoving = false;
            }
        }
        else
        {
            transform.localRotation = Quaternion.Euler(8, 180, 0);
            transform.Translate(Vector3.left * Time.deltaTime * Speed / (Weight * 10));
            if(transform.position.x >= movingRight.x)
            {
                isMoving = true;
            }
        }
    }
}

