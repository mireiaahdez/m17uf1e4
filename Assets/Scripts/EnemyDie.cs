﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDie : MonoBehaviour
{
    private int scorePoints = 2;

    public void OnMouseDown()
    {
        Destroy(this.gameObject);
        GameManager.Instance.IncreaseScore(scorePoints);
        GameManager.Instance.refreshEnemy(-1);
    }
}
