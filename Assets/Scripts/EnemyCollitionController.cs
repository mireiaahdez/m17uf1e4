﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollitionController : MonoBehaviour
{
    
    [SerializeField]
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("I'm the enemy");


        if (collision.CompareTag("Player"))
        {
            
            Debug.Log("Collision with the player");

        
            GameObject pl = collision.gameObject;

            if (pl.GetComponent<DataPlayer>().lifes <= 0)
            {
                Destroy(pl);
            }


        }

        Destroy(gameObject);


    }
}
