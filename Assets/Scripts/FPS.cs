﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPS : MonoBehaviour
{

    [SerializeField]
    private Text fpsText;
    private float refreshHud = 1f;
    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        int fps = (int)(1f / Time.unscaledDeltaTime);
        fpsText.text = "FPS: " + fps;
        timer = Time.unscaledTime + refreshHud;
        
    }
}
