﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;
    private List<GameObject> enemies;

    public Vector2 randomX;
    public Vector2 randomY;
    float spawnRate = 3f;
    float nextSpawn = 0.0f;
    float screenHeight = Screen.height;
    float randY;

    Vector2 spawn;
    // Start is called before the first frame update
    void Start()
    {
        randomX = new Vector2(1, 1);
        randomY = new Vector2(1, 1);

        //InvokeRepeating("CreateEnemy", 1, 5);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            randY = Random.Range(-4, 4);
            spawn = new Vector2(transform.position.x, randY);
            Instantiate(enemy, spawn, Quaternion.identity);
        }
    }


    /*private void CreateEnemy()
    {
        GameObject e = Instantiate(enemy, new Vector3(Random.RandomRange(randomX.x, randomY.y), Random.RandomRange(randomX.x, randomY.y), 0), Quaternion.identity);
        enemies.Add(e);
    }*/

}
