﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public Transform playerTransform;
    public DataPlayer dataPlayer;

    public Sprite[] sprites = new Sprite[3];


    public float distanceToX;
    public float distanceToY;
    public bool isMoving;
    private bool changeDirection = true;


    public SpriteRenderer spriteRenderer;

    int spriteIndex = 0;


    // Start is called before the first frame update
    void Start()
    {
        dataPlayer = gameObject.GetComponent<DataPlayer>();


        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprites[0];

        Application.targetFrameRate = 12;

        distanceToX = 0;
        distanceToY = 0;

        changeDirection = true;


        if (dataPlayer.Weight <= 10f)
        {
            dataPlayer.Speed = 0.8f;
        }
        else if (dataPlayer.Weight <= 25f)
        {
            dataPlayer.Speed = 0.4f;
        }
        else if (dataPlayer.Weight <= 40f)
        {
            dataPlayer.Speed = 0.1f;
        }
        else
        {
            dataPlayer.Speed = 0.01f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (changeDirection)
        {
            if (isMoving == true)
            {

                spriteRenderer.sprite = sprites[spriteIndex];

                if (spriteIndex == 2)
                {
                    spriteIndex = 1;
                }
                else
                {
                    spriteIndex++;
                }

                if (distanceToX >= dataPlayer.distance)
                {
                    spriteRenderer.flipX = true;
                    spriteRenderer.sprite = sprites[0];
                    changeDirection = false;

                    distanceToX = 0;
                }
                else
                {
                    transform.position = new Vector3(transform.position.x - dataPlayer.Speed, transform.position.y);
                    distanceToX += dataPlayer.Speed;
                }
            }
            else
            {
                Debug.Log(spriteRenderer.sprite);
                spriteRenderer.sprite = sprites[0];
            }
        }
        else
        {
            if (isMoving == true)
            {
                spriteRenderer.sprite = sprites[spriteIndex];

                if (spriteIndex == 2)
                {
                    spriteIndex = 1;
                }
                else
                {
                    spriteIndex++;
                }

                if (distanceToY >= dataPlayer.distance)
                {
                    spriteRenderer.flipX = false;
                    spriteRenderer.sprite = sprites[0];
                    changeDirection = true;

                    distanceToY = 0;
                }
                else
                {
                    transform.position = new Vector3(transform.position.x + dataPlayer.Speed, transform.position.y);
                    distanceToY += dataPlayer.Speed;
                }
            }
            else
            {
                spriteRenderer.sprite = sprites[0];
            }
        }
    }
}
