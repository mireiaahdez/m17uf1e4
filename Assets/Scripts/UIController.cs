﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public GameObject Player;
    public Text PlayerLifesText;
    public Text Score;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        PlayerLifesText.text = "Lifes" + GameManager.Instance.Player.GetComponent<DataPlayer>().lifes;
        Score.text = "Score" + GameManager.Instance.Score;
    }
}
